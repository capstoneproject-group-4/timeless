import React from 'react';
import renderer from 'react-test-renderer';

import MembersBox from '..';

describe('MembersBox Snapshot', () => {
  it('renders', () => {
    const tree = renderer.create(<MembersBox />);

    expect(tree).toMatchSnapshot();
  });
});
