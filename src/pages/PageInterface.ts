import { History, LocationState } from 'history';
import React, { ChangeEvent } from 'react';
import Record, { DeepMap, FieldError } from 'react-hook-form';

import * as Interface from '../redux/actions/interface.action';

export interface LandingPageProps {
  currentFrame: string;
  setCurrentFrame: React.Dispatch<React.SetStateAction<string>>;
  handleError: (message: string) => void;
  windowSize: {
    width: number;
    height: number;
  };
  handleForm: boolean;
  setHandleForm: React.Dispatch<React.SetStateAction<boolean>>;
}

export interface LoginProps {
  OnFinishLogin: (data: { email: string; password: string }) => void;
  windowSize: {
    width: number;
    height: number;
  };
  handleSubmit: any;
  errors: DeepMap<Record<string, any>, FieldError>;
  handleForm: boolean;
  handleChangeEmail: (evt: ChangeEvent<HTMLInputElement>) => void;
  handleChangePassword: (evt: ChangeEvent<HTMLInputElement>) => void;
}

export interface RegisterProps {
  windowSize: {
    width: number;
    height: number;
  };
  handleForm: boolean;
  setHandleForm: React.Dispatch<React.SetStateAction<boolean>>;
  handleSubmit: any;
  OnFinishRegister: (data: { name: string; email: string; password: string }) => void;
  errors: DeepMap<Record<string, any>, FieldError>;
  handleChangeName: (evt: ChangeEvent<HTMLInputElement>) => void;
  handleChangeEmail: (evt: ChangeEvent<HTMLInputElement>) => void;
  handleChangePassword: (evt: ChangeEvent<HTMLInputElement>) => void;
  switchText: any;
  setSwitchText: any;
}

export interface BoardProps {
  data: {
    windowSize?: {
      width: number;
      height: number;
    };
    showMobileMenu?: boolean;
    currentCard: any;
    showEditUser: boolean;
    showEditCard: boolean;
    showEditModal: boolean;
    showBoardModal: boolean;
    setCurrentCard: React.Dispatch<React.SetStateAction<object>>;
    setShowEditUser: React.Dispatch<React.SetStateAction<boolean>>;
    setShowEditCard: React.Dispatch<React.SetStateAction<boolean>>;
    setShowEditModal: React.Dispatch<React.SetStateAction<boolean>>;
    setShowBoardModal: React.Dispatch<React.SetStateAction<boolean>>;
    toggleMenu: () => void;
    selectedCard: {
      group: boolean;
      removeCard: boolean;
      fastCard: boolean;
      addText: boolean;
      connect: boolean;
      followedCard: boolean;
      blockedCard: boolean;
    };
    setSelectedCard: React.Dispatch<
      React.SetStateAction<{
        group: boolean;
        removeCard: boolean;
        fastCard: boolean;
        addText: boolean;
        connect: boolean;
        followedCard: boolean;
        blockedCard: boolean;
      }>
    >;
  };
  connection: {
    cardOne: boolean | number;
    setCardOne: React.Dispatch<React.SetStateAction<boolean>>;
    cardTwo: boolean | number;
    setCardTwo: React.Dispatch<React.SetStateAction<boolean>>;
    cardSelected: boolean;
    setCardSelected: React.Dispatch<React.SetStateAction<boolean>>;
    confirmConnection: boolean;
    setconfirmConnection: React.Dispatch<React.SetStateAction<boolean>>;
  };
  values: {
    cards: Interface.CardInterface[];
    history: History<LocationState>;
  };
  forceRerender: () => void;
  lines: any;
  setLines: any;
  defProps: {
    consoleWarning: boolean;
    passProps: {
      className: string;
      onMouseEnter: () => void;
      onMouseLeave: () => void;
      cursor: string;
    };
  };
  state: object;
  user: Interface.UserInterface;
  token: string;
  backgroundImage: string;
  setBackground: React.Dispatch<React.SetStateAction<string>>;
}
